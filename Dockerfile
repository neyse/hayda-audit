FROM golang:1.16.3-alpine AS builder

RUN apk update \
    && apk upgrade \
    && apk add --no-cache git curl

ENV CGO_ENABLED 0

ENV TRIVY_VERSION 0.18.3

RUN curl -sSL https://github.com/aquasecurity/trivy/releases/download/v${TRIVY_VERSION}/trivy_${TRIVY_VERSION}_Linux-64bit.tar.gz | tar -zx -C /tmp

WORKDIR /build/

COPY go.mod .
COPY go.sum .

RUN go mod download

COPY . .

RUN go build -trimpath -o /usr/local/bin/main -ldflags="-s -w" /build/cmd/main.go

FROM alpine:3.13.4
COPY --from=builder /usr/local/bin/main /usr/local/bin/main
COPY --from=builder /tmp/trivy /usr/local/bin/trivy

RUN apk add --no-cache git

ENTRYPOINT ["/usr/local/bin/main"]
CMD ["server"]
