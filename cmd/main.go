package main

import (
	"os"

	"hayda-audit/cmd/hayda-audit/cmd"
)

func main() {
	rootCmd := cmd.GetRootCmd(os.Args[1:])

	if err := rootCmd.Execute(); err != nil {
		os.Exit(1)
	}
}
