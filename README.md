# Hayda Audit

## Installation

```shell
$ kubectl apply -k .deploy/patches
```

## Usage

```shell
$ curl http://hayda-audit:9090/metrics | grep trivy_vulnerabilities
trivy_vulnerabilities{fixedVersion="",image="docker.io/calico/node:v3.7.3",installedVersion="0.0~r131-2",pkgName="liblz4-1",severity="LOW",vulnerabilityId="CVE-2019-17543"} 1
trivy_vulnerabilities{fixedVersion="",image="docker.io/calico/node:v3.7.3",installedVersion="0.168-1",pkgName="libelf1",severity="LOW",vulnerabilityId="CVE-2018-16402"} 1
trivy_vulnerabilities{fixedVersion="",image="docker.io/istio/examples-bookinfo-details-v1:1.16.2",installedVersion="1.1.1d-0+deb10u3",pkgName="libssl-dev",severity="LOW",vulnerabilityId="CVE-2007-6755"} 1
trivy_vulnerabilities{fixedVersion="",image="docker.io/istio/examples-bookinfo-details-v1:1.16.2",installedVersion="1.1.1d-0+deb10u3",pkgName="libssl-dev",severity="LOW",vulnerabilityId="CVE-2010-0928"} 1
trivy_vulnerabilities{fixedVersion="2.4.47+dfsg-3+deb10u5",image="registry.trendyol.com/platform/base/image/falcosecurity/falco:0.26.2",installedVersion="2.4.47+dfsg-3+deb10u3",pkgName="libldap-common",severity="HIGH",vulnerabilityId="CVE-2020-36229"} 1
```
